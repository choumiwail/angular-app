import { Rdv } from './../../../model/model.rdv';
import { RendezVousService } from './../../service/rendez-vous.service';
import { SousEntite } from './../../../model/model.sous-entite';
import { SousEntiteService } from './../../service/sous-entite.service';
import { CompteService } from './../../service/compte.service';
import { Router } from '@angular/router';
import { Component, OnInit, Input, ViewChild, OnChanges } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbPanelChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { Compte } from '../../../model/model.compte';
import { Entite } from '../../../model/model.entite';
import { EntiteService } from '../../service/entite.service';
import { Client } from '../../../model/model.client';
import { ClientService } from '../../service/client.service';
import { Acheteur } from '../../../model/model.acheteur';
import { AcheteurService } from '../../service/acheteur.service';
import { SuiviService } from '../../service/suivi.service';
import { Suivi } from '../../../model/model.suivi';
import { Contact } from '../../../model/model.contact';
import { DataService } from '../../service/data.service';
import { CommentaireService } from '../../service/commentaire.service';
import { Commentaire } from '../../../model/model.commentaire';


@Component({
  selector: 'app-sous-button',
  templateUrl: './sous-button.component.html',
  styleUrls: ['./sous-button.component.css']
})
export class SousButtonComponent implements OnInit{
  ListCompteComponent: any;
  @ViewChild('content') readonly createCompteModal;
  @ViewChild('contentEntite') readonly CreateEntiteModal;
  @ViewChild('contentSentite') readonly CreateSentiteModal;
  @ViewChild('contentClient') readonly CreateClientModal;
  @ViewChild('contentAcheteur') readonly CreateAcheteurModal;
  @ViewChild('contentBC') readonly CreateBCModal;
  @ViewChild('contentBesoin') readonly CreateBesoinModal;  
  /* @ViewChild('acch') readonly accordion: any; */
  
  @Input() optionClass: string;
  @Input() options: string;
  @Input() selectorId: string;
  @Input() icon: string;
  count : number;
  /* @Output() onEdit: EventEmitter<void> = new EventEmitter<void>(); */
  closeResult: string;
  getNumClient : any;
  etatBesoin : any;
  
  today = Date.now();


  fixedTimezone = '2015-06-15T09:03:01+0900';
  
  
  codecompter: string ;
  
  nentiter : string;
  pageComptes : any;
  pageEntites : any;
  pageSEntites : any;
  pageAcheteurs : any;
  pageClients : any;
  pageSuivis : any;
  pageCommentaire : any;


  /* codecpt : string ="C"; */
  codecpt = [{ name: "C",name1: "E",name2: "SE",name3: "A",name4: "CL",name5: "S"}];
    
  
  ii = [{ id: 1, name: "fa fa-table" },
  { id: 2, name: "fa fa-plus-square" },
  { id: 3, name: "fa fa-file-text-o" },];
  /* etats = [
    { value:1, checked:true},
    { value:0, checked:false}
  ] */
  /*   iii=[ {id:4,name:"fa fa-search"},
          {id:5,name:"fa fa-close"},
          {id:6,name:"fa fa-file-text-o"},] ;   */
  /*  ii= [1,2,3] ; */
  //Array.from(Array(6),(x,i)=> i+1);


  constructor(private router: Router,
              private modalService: NgbModal,
              public compteService : CompteService,
              public entiteService : EntiteService,
              public sousEntiteService : SousEntiteService,
              public clientService : ClientService,
              public acheteurService : AcheteurService,
              public suiviService : SuiviService,
              public rendezVousService : RendezVousService,
              public data : DataService,
              public commentaireService : CommentaireService) { }
              /*  selectedOptions() { 
              return this.etats
                        .filter(opt => opt.checked)
                        .map(opt => opt.value)
            } */

  ngOnInit() {
    
  }
 
  chekedBtn(selectorIds, typeOfCommmercial,codecompte) {
    //let index= this.ii.indexOf(selectorId);
    let index = this.ii[selectorIds];
    console.log(this.ii[selectorIds]);
    if (index.id === 2 && typeOfCommmercial === 'compte') {
      this.open(this.createCompteModal);
       /* console.log("Methode get: "+this.count) */
       this.data.currentValue.subscribe(count => this.count = count)
              
    }
    if (index.id === 1 && typeOfCommmercial === 'compte') {
      this.router.navigate(['/commercial-acceuil/list-compte']);
      
    }
    if (index.id === 2 && typeOfCommmercial === 'entite') {
      this.open(this.CreateEntiteModal);
      this.getListCompte();
      this.data.currentValue.subscribe(count => this.count = count)
    }
    if (index.id === 1 && typeOfCommmercial === 'entite') {
      this.router.navigate(['/commercial-acceuil/list-entite']);
      

    }
    if (index.id === 2 && typeOfCommmercial === 'sousentite') {
      this.open(this.CreateSentiteModal);
      this.getListCompte();
      this.data.currentValue.subscribe(count => this.count = count)

    }
    if (index.id === 1 && typeOfCommmercial === 'sousentite') {
      this.router.navigate(['/commercial-acceuil/list-sous-entite']);

    }
    if (index.id === 2 && typeOfCommmercial === 'client') {
      this.open(this.CreateClientModal);
      this.getListCompte();
      this.getAllAcheteur();
      this.data.currentValue.subscribe(count => this.count = count)

    }
    if (index.id === 1 && typeOfCommmercial === 'client') {
      this.router.navigate(['/commercial-acceuil/list-client']);

    }
    if (index.id === 2 && typeOfCommmercial === 'acheteur') {
      this.open(this.CreateAcheteurModal);
      this.data.currentValue.subscribe(count => this.count = count)
      
    }
    if (index.id === 1 && typeOfCommmercial === 'acheteur') {
      this.router.navigate(['/commercial-acceuil/list-acheteur']);

    }
    if (index.id === 2 && typeOfCommmercial === 'basecommerciale') {
      this.open(this.CreateBCModal);
      this.getAllClient();
      this.getAllSuivi();
      this.data.currentValue.subscribe(count => this.count = count)
    }
    if (index.id === 1 && typeOfCommmercial === 'basecommerciale') {
      this.router.navigate(['/commercial-acceuil/list-base-commerciale']);

    }
    if (index.id === 2 && typeOfCommmercial === 'besoin') {
      this.open(this.CreateBesoinModal);
      
    }
    if (index.id === 1 && typeOfCommmercial === 'besoin') {
      //this.router.navigate(['/commercial-acceuil/list-base-commerciale']);

    }
    /* else { console.log('Error') }*/ 
  
  }

  open(content) {
    this.modalService.open(content,{ size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
    
  newValue() {
    this.data.changeValue(++this.count)
  }
 
  

  valueOfCpt(){
    console.log("val"+this.count)
    
  }

  description :string =""
  vider(){
    this.description="";
  }

  /* Method of class Compte*/
  saveCompte(compte : Compte){
    this.compteService.saveCompte(compte)
      .subscribe(data => {
        console.log(data)
        this.router.navigate(['/commercial-acceuil/list-compte']);
      }, err => {
        console.log(err);
      })
  }

  getListCompte(){
    this.compteService.getAllComptes()
      .subscribe(data => {
        this.pageComptes = data;
        console.log(data);
      }, err => {
        console.log(err);
      });
  }
  /* valOfCpt : Compte = new Compte();
  getCompte(codecompte){
    this.compteService.getCompte(codecompte)
      .subscribe(data => {
        console.log(data);
         this.valOfCpt = data
      }, err => {
        console.log(err);
      });
  } */
  /* End*/

  /* Method of class Entite*/
  getAllEntite(){
    this.entiteService.getAllEntite()
      .subscribe(data => {
        this.pageEntites = data;
        console.log(data);
      }, err => {
        console.log(err);
      });
  }

  saveEntite(entite : Entite){
    this.entiteService.saveEntite(entite)
      .subscribe(data => {
        console.log(data)      
        this.router.navigate(['/commercial-acceuil/list-entite']);
      }, err => {
        console.log(err);
      })
  }
  /* End */

  /* Method of class Sous entite*/
  getAllSEntite(){
    this.sousEntiteService.getAllSEntite()
      .subscribe(data => {
        this.pageSEntites = data;
        console.log(data);
      }, err => {
        console.log(err);
      });
  }

  saveSousEntite(sousentite : SousEntite){
    this.sousEntiteService.saveSousEntite(sousentite)
      .subscribe(data => {
        console.log(data)
        this.router.navigate(['/commercial-acceuil/list-sous-entite']);
      }, err => {
        console.log(err);
      })
  }

  /* End */

  /* Method of class Acheteur*/
  getAllAcheteur(){
    this.acheteurService.getAllAcheteur()
      .subscribe(data => {
        this.pageAcheteurs = data;
        console.log(data);
      }, err => {
        console.log(err);
      });
  }
  
  saveAcheteur(acheteur : Acheteur){
    this.acheteurService.saveAcheteur(acheteur)
      .subscribe(data => {
        console.log(data)
      }, err => {
        console.log(err);
      })
  }

  pgAcheteur : Acheteur = new Acheteur;
  getAcheteur(/* matriculeacheteur : string */){
    console.log("nacheteur:"+ this.codecpt[0].name3+""+this.count)
    /* console.log("nacheteur:"+ this.codecpt[0].name3+""+this.count) */
    this.acheteurService.getAcheteur(this.codecpt[0].name3+""+this.count)
      .subscribe(data => {
        console.log("data:" + data)
        this.pgAcheteur = data
        console.log("///" +this.pgAcheteur.matriculeacheteur)
      }, err => {
        console.log(err);
      })
  }
  /* End */

  /* Method of class Commentaire*/
  getAllCommentaire(){
    this.commentaireService.getAllCommentaire()
      .subscribe(data => {
        this.pageCommentaire = data;
        console.log(data)
      }, err => {
        console.log(err)
      })
  }


  /* commentaire : Commentaire = new Commentaire(); */
  saveCommentaire(commentaire : Commentaire){
    this.commentaireService.saveCommentaire(commentaire)
      .subscribe(data => {
        console.log(data)
        this.router.navigate(['/commercial-acceuil/list-acheteur']);
      }, err => {
        console.log(err)
      })
  }
  /* End */

  /* Method of class Client*/
  getAllClient(){
    this.clientService.getAllClient()
      .subscribe(data => {
        this.pageClients = data;
        console.log(data);
      }, err => {
        console.log(err);
      });
  }

  saveClient(client : Client){
    this.clientService.saveClient(client)
      .subscribe(data => {
        console.log(data)
        this.router.navigate(['/commercial-acceuil/list-client']);
      }, err => {
        console.log(err);
      })
  }
  /* End */
  
  /* Method of class suivi*/
  getAllSuivi(){
    this.suiviService.getAllSuivi()
      .subscribe(data => {
        this.pageSuivis = data;
        console.log(data);
      }, err => {
        console.log(err);
      });
  }

  saveContact(suivi : Suivi){
    this.suiviService.saveSuivi(suivi)
      .subscribe(data => {
        console.log(data)
      }, err => {
        console.log(err);
      })
  }

  updateSuivi(suiv : Suivi){
    this.suiviService.updateSuivi(suiv)
      .subscribe(data => {
        console.log(data)
      }, err => {
        console.log(err)
      })
  }

  codesuivi: string;

  
  showUpdate:boolean = false;
  showCreate:boolean = true;
  pgsuivi : Suivi = new Suivi();
  receiveSuivi($event) {
    this.showUpdate = !this.showUpdate;
    this.showCreate = !this.showCreate;
    this.pgsuivi = $event
    console.log("/*/"+this.pgsuivi.numsuivi);
  }
  /* End */

  /* Method of class rdv*/
  saveRdv(rdv : Rdv){
    this.rendezVousService.saveRdv(rdv)
      .subscribe(data => {
        console.log(data)
        this.router.navigate(['/commercial-acceuil/list-base-commerciale']);
      }, err => {
        console.log(err)
      })
  }
  /* End */
  getClient(codeclient : string){
    console.log(codeclient);
    console.log("Console : "+codeclient);
    this.clientService.getClient(codeclient)
      .subscribe(data => {
        console.log("------ : "+codeclient);
      })
  }

  getNumCompte : any;
  pageOfEntites : any;

  getListOfEntite(){
    console.log("My input: ", this.getNumCompte);
    this.sousEntiteService.getListOfEntite(this.getNumCompte)
      .subscribe(data => {
        this.pageOfEntites= data;
      }, err => {
        console.log(err);
      })
  }

  getNum : any;
  pageOfSE : any;

  getListOfSE(){
    console.log("My input: ", this.getNum);
    this.clientService.getListOfSE(this.getNum)
      .subscribe(data => {
        this.pageOfSE= data;
      }, err => {
        console.log(err);
      })
  }


  
  /* getListOfCompte(){
    console.log("My input: ", this.getNumClient);
    this.suiviService.getListOfCompte(this.getNumClient)
      .subscribe(data => {
        this.pageSuivis= data;
      }, err => {
        console.log(err);
      })
  } */

  /* saveSuivi(suivi : Suivi){
    this.suiviService.saveSuivi(suivi)
      .subscribe(data => {
        console.log("****"+data)
        console.log("-----"+suivi)
      }, err => {
        console.log(err);
      })
  } */

  

  


}
