import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SousButtonComponent } from './sous-button.component';

describe('SousButtonComponent', () => {
  let component: SousButtonComponent;
  let fixture: ComponentFixture<SousButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SousButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SousButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
