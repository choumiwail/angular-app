import { AcheteurService } from './../../service/acheteur.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Acheteur } from '../../../model/model.acheteur';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Http } from '@angular/http';
import { DataService } from '../../service/data.service';
import { CommentaireService } from '../../service/commentaire.service';

@Component({
  selector: 'list-acheteur',
  templateUrl: './list-acheteur.component.html',
  styleUrls: ['./list-acheteur.component.css']
})
export class ListAcheteurComponent implements OnInit,AfterViewInit  {

  pageAcheteurs : any;
  data;
  count : number;
  @ViewChild('contentAcheteur') readonly createCompteModal;
   closeResult: string;
   acheteur : Acheteur = new Acheteur();

  constructor(public http : Http,
              private modalService: NgbModal,
              public acheteurService: AcheteurService,
              public commService: CommentaireService,
              public dataService : DataService) { }

  ngOnInit() {
    this.dataService.currentValue.subscribe(count => this.count = count)
  }

  newValue() {
    this.dataService.changeValue(this.count)
  }

  ngAfterViewInit(){
    this.getAllAcheteur();
  }

  getAllAcheteur(){
    this.commService.getAllCommentaire()
      .subscribe(data => {
        this.pageAcheteurs = data;
        console.log(data);
      }, err => {
        console.log(err);
      });
  }

  /* getAllAchComm(){
    this.commService.getAllAchComm("A6")
      .subscribe(data => {
        this.pageAcheteurs = data;
        console.log(data);
      }, err => {
        console.log(err);
      });
  } */

  openContent(nacheteur){
    
    this.open(this.createCompteModal);
    //this.compte.codecompte;
    console.log("Console : "+nacheteur);
    this.acheteurService.getAcheteur(nacheteur)
      .subscribe(data => {
        console.log("------ : "+nacheteur);
        this.acheteur = data;
      })
  }

  updateAcheteur(){
    this.acheteurService.updateAcheteur(this.acheteur)
      .subscribe(data => {
        console.log(data)
      }, err => {
        console.log(err)
      })
  }

  open(content) {
    this.modalService.open(content,{ size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
