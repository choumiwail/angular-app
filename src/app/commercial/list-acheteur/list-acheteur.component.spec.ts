import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAcheteurComponent } from './list-acheteur.component';

describe('ListAcheteurComponent', () => {
  let component: ListAcheteurComponent;
  let fixture: ComponentFixture<ListAcheteurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAcheteurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAcheteurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
