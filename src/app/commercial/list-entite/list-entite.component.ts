import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { EntiteService } from '../../service/entite.service';
import { Http } from '@angular/http';
import { Entite } from '../../../model/model.entite';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { CompteService } from '../../service/compte.service';
import { DataService } from '../../service/data.service';

@Component({
  selector: 'app-list-entite',
  templateUrl: './list-entite.component.html',
  styleUrls: ['./list-entite.component.css']
})
export class ListEntiteComponent implements OnInit,AfterViewInit {

  pageEntites : any;
  pageComptes : any;
  data;
  count : number;
  @ViewChild('contentEntite') readonly createCompteModal;
   closeResult: string;
   entite : Entite = new Entite();

  constructor(public http : Http, public entiteService : EntiteService,
              private modalService: NgbModal,
              public compteService : CompteService,
              public dataService : DataService) { }

  ngOnInit() {
    this.dataService.currentValue.subscribe(count => this.count = count)
  }

  newValue() {
    this.dataService.changeValue(this.count)
  }

  ngAfterViewInit(){
    this.getAllEntite();
  }

  getAllEntite(){
    this.entiteService.getAllEntite()
      .subscribe(data => {
        this.pageEntites = data;
        console.log(data);
      }, err => {
        console.log(err);
      });
  }
  getListCompte(){
    this.compteService.getAllComptes()
      .subscribe(data => {
        this.pageComptes = data;
        console.log(data);
      }, err => {
        console.log(err);
      });
  }

  openContent(nentite){
    
    this.open(this.createCompteModal);
    //this.compte.codecompte;
    this.getListCompte();
    console.log("Console : "+nentite);
    this.entiteService.getEntite(nentite)
      .subscribe(data => {
        console.log("------ : "+nentite);
        this.entite = data;
      })
  }

  updateEntite(){
    this.entiteService.updateEntite(this.entite)
      .subscribe(data => {
        console.log(data)
      }, err => {
        console.log(err)
      })
  }

  open(content) {
    this.modalService.open(content,{ size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
