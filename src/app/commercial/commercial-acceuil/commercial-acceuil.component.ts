import { Component, OnInit } from '@angular/core';
import { Employeur } from '../../../model/Utilisateur/model.employeur';

@Component({
  selector: 'app-commercial-acceuil',
  templateUrl: './commercial-acceuil.component.html',
  styleUrls: ['./commercial-acceuil.component.css']
})
export class CommercialAcceuilComponent implements OnInit {
  currentUser : Employeur;

  constructor() { 
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
  }

}
