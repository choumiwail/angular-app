import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommercialAcceuilComponent } from './commercial-acceuil.component';

describe('CommercialAcceuilComponent', () => {
  let component: CommercialAcceuilComponent;
  let fixture: ComponentFixture<CommercialAcceuilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommercialAcceuilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommercialAcceuilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
