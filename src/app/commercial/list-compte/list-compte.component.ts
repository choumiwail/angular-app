import { Http } from '@angular/http';
import { Component, OnInit,AfterViewInit, ViewChild } from '@angular/core';
import { CompteService } from '../../service/compte.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Compte } from '../../../model/model.compte';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../../service/data.service';

@Component({
  selector: 'app-list-compte',
  templateUrl: './list-compte.component.html',
  styleUrls: ['./list-compte.component.css']
})
export class ListCompteComponent implements OnInit,AfterViewInit {

   pageComptes : any; 
   /* pgComptes : Compte = new Compte(); */
   pgComptes : any;   
   motCle : string = "";
   page : number = 0;
   size : number = 5;
   pages : Array<number>;
   data;
   count : number;
   @ViewChild('content') readonly createCompteModal;
   closeResult: string;
   compte : Compte = new Compte();

  constructor(public http : Http, public compteService : CompteService,
              private modalService: NgbModal/* ,
              public actived : ActivatedRoute */,
              public dataService : DataService) {
               }

  ngOnInit() {
    this.dataService.currentValue.subscribe(count => this.count = count)
  }

  newValue() {
    this.dataService.changeValue(this.count)
  }

  getListCompte(){
    this.compteService.getAllComptes()
      .subscribe(data => {
        this.pageComptes = data;
        console.log(data);
      }, err => {
        console.log(err);
      });
  }

  openContent(codecompte){
    
    this.open(this.createCompteModal);
    //this.compte.codecompte;
    console.log("Console : "+codecompte);
    this.compteService.getCompte(codecompte)
      .subscribe(data => {
        console.log("------ : "+codecompte);
        this.compte = data;
      })
  }

  updateCompte(){
    this.compteService.updateCompte(this.compte)
      .subscribe(data => {
        console.log(data)
      }, err => {
        console.log(err)
      })
  }

  open(content) {
    this.modalService.open(content,{ size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  ngAfterViewInit(){
   // this.getListCompte();
    this.doSearch();

  }

  /* getListCompte(){
    this.compteService.getAllComptes()
      .subscribe(data => {
        this.pageComptes = data;
        console.log(data);
      }, err => {
        console.log(err);
      });
  } */

  doSearch(){
    this.compteService.getComptes(this.motCle/* ,this.page,this.size */)
      .subscribe(data => {
        this.pgComptes = data;
        this.pages = new Array(data.totalPages);
        console.log("data of chercher" + this.pgComptes)
        console.log("data of pg" + this.pages)
      }, err => {
        console.log(err);
      });
  }

  chercher(){
     this.doSearch();
  }

  /*goToPages(i : number){
    this.page = i;
    this.doSearch();
  } */

}
