import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-toogle-button',
  templateUrl: './toogle-button.component.html',
  styleUrls: ['./toogle-button.component.css']
})
export class ToogleButtonComponent implements OnInit, AfterViewInit {
  @Input() matClass: string;
  @Input() option: string;

  constructor() { }

  ngOnInit() {
  }

   ngAfterViewInit() {
      $(`.${this.matClass}`).on("click", () => {
        $(this).toggleClass('open');
        $(`.${this.option}`).toggleClass('scale-on');
    });
    } 
}
