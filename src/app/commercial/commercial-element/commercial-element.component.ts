import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-commercial-element',
  templateUrl: './commercial-element.component.html',
  styleUrls: ['./commercial-element.component.css']
})
export class CommercialElementComponent implements OnInit {
  @Input() title: string;
  @Input() selectorId: string;
  @Input() image: string;


  constructor() { }

  ngOnInit() {
  }

}
