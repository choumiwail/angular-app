import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommercialElementComponent } from './commercial-element.component';

describe('CommercialElementComponent', () => {
  let component: CommercialElementComponent;
  let fixture: ComponentFixture<CommercialElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommercialElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommercialElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
