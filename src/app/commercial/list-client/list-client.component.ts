import { AcheteurService } from './../../service/acheteur.service';
import { ClientService } from './../../service/client.service';
import { Client } from './../../../model/model.client';
import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from '../../service/data.service';

@Component({
  selector: 'app-list-client',
  templateUrl: './list-client.component.html',
  styleUrls: ['./list-client.component.css']
})
export class ListClientComponent implements OnInit,AfterViewInit  {

  pageClients : any;
  pageAcheteurs : any;
  data;
  count : number;
  @ViewChild('contentClient') readonly createCompteModal;
   closeResult: string;
   client : Client = new Client();

   constructor(public http : Http,
    private modalService: NgbModal,
    public clientService: ClientService,
    public acheteurService: AcheteurService,
    public dataService : DataService) { }

    ngOnInit() {
      this.dataService.currentValue.subscribe(count => this.count = count)
    }
  
    newValue() {
      this.dataService.changeValue(this.count)
    }

  ngAfterViewInit(){
    this.getAllClient();
  }

  getAllClient(){
    this.clientService.getAllClient()
      .subscribe(data => {
        this.pageClients = data;
        console.log(data);
      }, err => {
        console.log(err);
      });
  }
  getAllAcheteur(){
    this.acheteurService.getAllAcheteur()
      .subscribe(data => {
        this.pageAcheteurs = data;
        console.log(data);
      }, err => {
        console.log(err);
      });
  }

  openContent(nclient){
    
    this.open(this.createCompteModal);
    this.getAllAcheteur();
    //this.compte.codecompte;
    console.log("Console : "+nclient);
    this.clientService.getClient(nclient)
      .subscribe(data => {
        console.log("------ : "+nclient);
        this.client = data;
      })
  }

  updateClient(){
    this.clientService.updateClient(this.client)
      .subscribe(data => {
        console.log(data)
      }, err => {
        console.log(err)
      })
  }

  open(content) {
    this.modalService.open(content,{ size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
