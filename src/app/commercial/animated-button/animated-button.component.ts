import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-animated-button',
  templateUrl: './animated-button.component.html',
  styleUrls: ['./animated-button.component.css']
})
export class AnimatedButtonComponent implements OnInit, AfterViewInit {
  @Input() selectorId: string;
  @Input() materialBtnTog: string;
  @Input() option: string; 

  


  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    $(`.${this.materialBtnTog}`).on("click", () => {
      $(this).toggleClass('open');
      $(`.${this.option}`).toggleClass('scale-on');
  });
  }


 /*  ngAfterViewInit() {
      $(`.${this.materialBtnTog}`).on("click", () => {
        $(this).toggleClass('open');
        $(`.${this.option}`).toggleClass('scale-on');
    });
    }  */



}
