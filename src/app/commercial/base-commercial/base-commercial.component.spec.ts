import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseCommercialComponent } from './base-commercial.component';

describe('BaseCommercialComponent', () => {
  let component: BaseCommercialComponent;
  let fixture: ComponentFixture<BaseCommercialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseCommercialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseCommercialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
