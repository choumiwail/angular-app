import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSousEntiteComponent } from './list-sous-entite.component';

describe('ListSousEntiteComponent', () => {
  let component: ListSousEntiteComponent;
  let fixture: ComponentFixture<ListSousEntiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListSousEntiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSousEntiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
