import { EntiteService } from './../../service/entite.service';
import { CompteService } from './../../service/compte.service';
import { Http } from '@angular/http';
import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { SousEntiteService } from '../../service/sous-entite.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { SousEntite } from '../../../model/model.sous-entite';
import { DataService } from '../../service/data.service';

@Component({
  selector: 'list-sous-entite',
  templateUrl: './list-sous-entite.component.html',
  styleUrls: ['./list-sous-entite.component.css']
})
export class ListSousEntiteComponent implements OnInit,AfterViewInit {

  pageSEntites : any;
  pageEntites : any;
  pageComptes : any;
  data;
  count : number;
  @ViewChild('contentSentite') readonly createCompteModal;
   closeResult: string;
   sousEntite : SousEntite = new SousEntite();

  constructor(public http : Http, public sEntiteService : SousEntiteService,
              private modalService: NgbModal,
              public compteService : CompteService,
              public entiteService : EntiteService,
              public dataService : DataService) { }

ngOnInit() {
  this.dataService.currentValue.subscribe(count => this.count = count)
 }
            
newValue() {
  this.dataService.changeValue(this.count)
 }

  ngAfterViewInit(){
    this.getListSEntite();
  }

  getListSEntite(){
    this.sEntiteService.getAllSEntite()
      .subscribe(data => {
        this.pageSEntites = data;
        console.log(data);
      }, err => {
        console.log(err);
      });
  }

  getListEntite(){
    this.entiteService.getAllEntite()
      .subscribe(data => {
        this.pageEntites = data;
        console.log(data);
      }, err => {
        console.log(err);
      });
  }
  getListCompte(){
    this.compteService.getAllComptes()
      .subscribe(data => {
        this.pageComptes = data;
        console.log(data);
      }, err => {
        console.log(err);
      });
  }

  openContent(nsentite){
    
    this.open(this.createCompteModal);
    //this.compte.codecompte;
    console.log("Console : "+nsentite);
    this.getListCompte();
    this.getListEntite();
    this.sEntiteService.getSEntite(nsentite)
      .subscribe(data => {
        console.log("------ : "+nsentite);
        this.sousEntite = data;
      })
  }

  updateSousEntite(){
    this.sEntiteService.updateSousEntite(this.sousEntite)
      .subscribe(data => {
        console.log(data)
      }, err => {
        console.log(err)
      })
  }

  open(content) {
    this.modalService.open(content,{ size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
