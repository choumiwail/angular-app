import { AcheteurService } from './../../service/acheteur.service';
import { Commentaire } from './../../../model/model.commentaire';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { CommentaireService } from '../../service/commentaire.service';
import { DataService } from '../../service/data.service';

@Component({
  selector: 'add-compte',
  templateUrl: './add-compte.component.html',
  styleUrls: ['./add-compte.component.css']
})
export class AddCompteComponent implements OnInit,AfterViewInit {

  @Input() matricule : string;
  @Input() count : number;
  @Output() event = new EventEmitter<any>();

  today = Date.now();
  pageCommentaire : any;
  closeResult: string;

  constructor(private router: Router,
              private modalService: NgbModal,
              public commentaireService : CommentaireService,
              public acheteurService : AcheteurService,
              public data : DataService) { }

  ngOnInit() {
    
  }
  
  ngAfterViewInit(){
    this.getAllAcheteur();
  }

  closeToogle(){
    this.event.emit();
  }

 /* Method of class Commentaire*/
 getAllCommentaire(){
  this.commentaireService.getAllCommentaire()
    .subscribe(data => {
      this.pageCommentaire = data;
      console.log(data)
    }, err => {
      console.log(err)
    })
}

saveCommentaire(commentaire : Commentaire){
  this.commentaireService.saveCommentaire(commentaire)
    .subscribe(data => {
      console.log(data)
      this.data.changeValue(++this.count);
      this.router.navigate(['/commercial-acceuil/list-acheteur']);
    }, err => {
      console.log(err)
    })
}
/* End */
  
   /* Method of class Acheteur*/
   pageAcheteurs : any;
   getAllAcheteur(){
    this.acheteurService.getAllAcheteur()
      .subscribe(data => {
        this.pageAcheteurs = data;
        console.log(data);
      }, err => {
        console.log(err);
      });
  }
  /* End */


}
