import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import * as moment from 'moment';
import {FR} from 'bankholiday';

@Component({
  selector: 'app-bkholiday',
  templateUrl: './bkholiday.component.html',
  styleUrls: ['./bkholiday.component.css']
})
export class BkholidayComponent implements OnInit {

  currentDate = moment();

  /* bankholiday = require('bankholiday').FR; */

  holidays = FR.findAll(this.currentDate.format('YYYY'));

  @Output() event = new EventEmitter<any>();
  @Output() eventclose = new EventEmitter<any>();
  /* dayNames = ['day1', 'day2', 'day3', 'day4', 'day5', 'day6', 'day7', 'day8', 'day9', 'day10', 'day11']; */

  

  constructor() { }

  ngOnInit() {
  }
  
  closeToogle(){
    this.event.emit();
  }

  clsToogle(){
    this.eventclose.emit();
  }

}
