import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BkholidayComponent } from './bkholiday.component';

describe('BkholidayComponent', () => {
  let component: BkholidayComponent;
  let fixture: ComponentFixture<BkholidayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BkholidayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BkholidayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
