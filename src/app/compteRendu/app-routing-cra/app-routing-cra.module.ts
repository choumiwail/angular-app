import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CraAcceuilComponent } from '../cra-acceuil/cra-acceuil.component';
import { CraComponent } from '../cra/cra.component';

const compCenterRoutes: Routes = [
  {
    path: 'cra-acceuil',
    component: CraAcceuilComponent,
    children:[
      {path: '', redirectTo: 'CRA', pathMatch: 'full'},
      {path: 'CRA', component: CraComponent},
      /* {path: 'list-compte', component: ListCompteComponent},*/
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(compCenterRoutes),
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingCraModule { }
