import { CompteRenduActivite } from './../../../model/CRA/model.compterenduactivite';
import { CraService } from './../../service/CRA/cra.service';
import { DataService } from './../../service/data.service';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild, ElementRef } from '@angular/core';
import * as moment from 'moment';
import * as _ from 'lodash';
import { Employeur } from '../../../model/Utilisateur/model.employeur';
import { Router } from '@angular/router';


@Component({
  selector: 'CRA',
  templateUrl: './cra.component.html',
  styleUrls: ['./cra.component.css'],
})


export class CraComponent implements OnInit, OnChanges {
  currentDate = moment();

  /* @ViewChild('jrs')  jrs:ElementRef; */

  /* bankholiday = require('bankholiday').FR;

  holidays = this.bankholiday.findAll(this.currentDate.format('YYYY')); */
  @ViewChild('contentbank') readonly createBankHoliday;
  dayNames = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
  weeks: CompteRenduActivite[][] = [];
  sortedDates: CompteRenduActivite[] = [];

  @Input() selectedDates: CompteRenduActivite[] = [];
  @Output() onSelectDate = new EventEmitter<CompteRenduActivite>();
  currentUser: Employeur;
  values = '';

  constructor(private router: Router,
    private dataService: DataService,
    private craService: CraService) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit(): void {
    /* this.generateCalendar(); */
    this.initDaysInMonthArray();
    this.values = this.currentUser.username;

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.selectedDates &&
      changes.selectedDates.currentValue &&
      changes.selectedDates.currentValue.length > 1) {
      // sort on date changes for better performance when range checking
      /* this.sortedDates = _.sortBy(changes.selectedDates.currentValue, (m: CalendarDate) => m.mDate.valueOf());
      this.generateCalendar(); */
    }
  }


  // date checkers

  isToday(date: moment.Moment): boolean {
    return moment().isSame(moment(date), 'day');
  }

  isSelected(date: moment.Moment): boolean {
    return _.findIndex(this.selectedDates, (selectedDate) => {
      return moment(date).isSame(selectedDate.mDate, 'day');
    }) > -1;
  }


  // generate the calendar grid

  initDaysInMonthArray() {
    const dayOfMonth = [];
    for (let index = 0; index < moment().daysInMonth(); index++) {
      const compte = new CompteRenduActivite(
        index + 1,
        '',
        '',
        '',
        '',
        7,
        '',
        '',
        '',
        this.values = this.currentUser.username
      );
      dayOfMonth.push(compte);
    }
    const weeks: CompteRenduActivite[][] = [];
    while (dayOfMonth.length > 0) {
      weeks.push(dayOfMonth.splice(0, 7));
    }
    this.weeks = weeks;
  }

  bkholiday() {
    this.dataService.openMd(this.createBankHoliday);
  }

  /* Method of class CRA */
  saveCra() {
    const compteRendu: CompteRenduActivite[] = _.flatten(this.weeks);
    console.log(compteRendu);
    
    this.craService.saveCra(compteRendu)
      .subscribe(data => {
        console.log(data)
      }, err => {
        console.log(err)
      })
  }

  /* End */

}
