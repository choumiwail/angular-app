import { Component, OnInit } from '@angular/core';
import { Employeur } from '../../../model/Utilisateur/model.employeur';

@Component({
  selector: 'cra-acceuil',
  templateUrl: './cra-acceuil.component.html',
  styleUrls: ['./cra-acceuil.component.css']
})
export class CraAcceuilComponent implements OnInit {
  currentUser : Employeur;

  constructor() { 
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
  }

}
