import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CraAcceuilComponent } from './cra-acceuil.component';

describe('CraAcceuilComponent', () => {
  let component: CraAcceuilComponent;
  let fixture: ComponentFixture<CraAcceuilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CraAcceuilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CraAcceuilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
