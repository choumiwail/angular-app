import { Employeur } from './../../../model/Utilisateur/model.employeur';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class EmployeurService {

  constructor(private http : Http) { }

  getAllEmployeur(){
    return this.http.get("http://localhost:8080/employeurs")
      .map(resp => resp.json());
  }

  getEmployeur(username : string){
    return this.http.get("http://localhost:8080/employeurs/"+username)
      .map(resp => resp.json());
  }

  saveEmployeur(employeur : Employeur){
    return this.http.post("http://localhost:8080/employeurs", employeur)
      .map(resp => resp.json());

  }

  updateEmployeur(employeur : Employeur){
    return this.http.put("http://localhost:8080/employeurs/"+employeur.username, employeur)
      .map(resp => resp.json());
  }
}
