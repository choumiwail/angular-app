import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Contact } from '../../model/model.contact';

@Injectable()
export class ContactService {

  constructor(public http : Http) { }

  getAllContact(){
    return this.http.get("http://localhost:8080/contacts")
      .map(resp => resp.json());
  }

  saveContact(contact : Contact){
    return this.http.post("http://localhost:8080/contacts" , contact)
      .map(resp => resp.json());
  }

}
