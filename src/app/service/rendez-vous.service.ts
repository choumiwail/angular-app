import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Rdv } from '../../model/model.rdv';

@Injectable()
export class RendezVousService {

  constructor(public http : Http) { }

  getAllRdv(){
    return this.http.get("http://localhost:8080/rendezvous")
      .map(resp => resp.json());
  }

  saveRdv(rdv : Rdv){
    return this.http.post("http://localhost:8080/rendezvous", rdv)
      .map(resp => resp.json());
  }

}
