import { Contrat } from './../../../model/Comptabilite/model.contrat';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ContratService {

  constructor(public http : Http) { }

  getAllContrat(){
    return this.http.get("http://localhost:8080/contrats")
      .map(resp => resp.json());
  }

  getContrat(ncontrat : string){
    return this.http.get("http://localhost:8080/contrats/"+ncontrat)
      .map(resp => resp.json());
  }

  
  saveContrat(contrat : Contrat){
    return this.http.post("http://localhost:8080/contrats", contrat)
      .map(resp => resp.json());

  }
}
