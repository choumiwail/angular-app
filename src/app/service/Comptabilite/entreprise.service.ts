import { Entreprise } from './../../../model/Comptabilite/model.entreprise';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class EntrepriseService {

  constructor(public http : Http) { }

  getAllEntreprise(){
    return this.http.get("http://localhost:8080/entreprises")
      .map(resp => resp.json());
  }

  getEntreprise(identreprise : number){
    return this.http.get("http://localhost:8080/entreprises/"+identreprise)
      .map(resp => resp.json());
  }

  saveEntreprise(entreprise : Entreprise){
    return this.http.post("http://localhost:8080/entreprises", entreprise)
      .map(resp => resp.json());

  }

  /* updateEntreprise(entreprise : Entreprise){
    return this.http.put("http://localhost:8080/entreprises/"+entreprise.identreprise, acheteur)
      .map(resp => resp.json());

  } */
}
