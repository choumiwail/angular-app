import { Consultant } from './../../../model/Comptabilite/model.consultant';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ConsultantService {

  constructor(public http : Http) { }

  getAllConsultant(){
    return this.http.get("http://localhost:8080/consultants")
      .map(resp => resp.json());
  }

  getConsultant(nconsultant : number){
    return this.http.get("http://localhost:8080/consultants/"+nconsultant)
      .map(resp => resp.json());
  }

  saveConsultant(consultant : Consultant){
    return this.http.post("http://localhost:8080/consultants", consultant)
      .map(resp => resp.json());

  }

}
