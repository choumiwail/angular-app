import { Facture } from './../../../model/Comptabilite/model.facture';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class FactureService {

  constructor(public http : Http) { }

  getAllFacture(){
    return this.http.get("http://localhost:8080/factures")
      .map(resp => resp.json());
  }

  getFacture(nfacture : number){
    return this.http.get("http://localhost:8080/factures/"+nfacture)
      .map(resp => resp.json());
  }

  saveFacture(facture : Facture){
    return this.http.post("http://localhost:8080/factures", facture)
      .map(resp => resp.json());

  }

}
