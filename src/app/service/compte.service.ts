import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Compte } from '../../model/model.compte';

@Injectable()
export class CompteService {
 
  constructor(public http: Http) { }


  getComptes(motCle : string/* , page : number, size : number */){
    return this.http.get("http://localhost:8080/comptes/ChercherComptes?mc="+motCle/* +"&size="+size+"&page="+page */)
      .map(resp => resp.json());
  }

  getCompte(codecompte:string){
    return this.http.get("http://localhost:8080/comptes/"+codecompte)
      .map(resp => resp.json());
  }

  getValOfCpt(){
    return this.http.get("http://localhost:8080/comptes/getValOfCpt")
     .map(resp => resp)
  }

  getAllComptes(){
    return this.http.get("http://localhost:8080/comptes")
      .map(resp => resp.json());
  }
 
  saveCompte(compte : Compte){
    return this.http.post("http://localhost:8080/comptes", compte)
      .map(resp => resp.json());
  }

  updateCompte(compte : Compte){
    return this.http.put("http://localhost:8080/comptes/"+compte.codecompte, compte)
      .map(resp => resp.json());

  }

  /* getListOfCompte(codeclient : string){
    return this.http.get("http://localhost:8080/comptes/getCompte?mc="+codeclient)
      .map(resp => resp.json());
  } */

}
