import { TestBed, inject } from '@angular/core/testing';

import { SousEntiteService } from './sous-entite.service';

describe('SousEntiteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SousEntiteService]
    });
  });

  it('should be created', inject([SousEntiteService], (service: SousEntiteService) => {
    expect(service).toBeTruthy();
  }));
});
