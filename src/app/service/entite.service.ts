import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Entite } from '../../model/model.entite';
import { Compte } from '../../model/model.compte';

@Injectable()
export class EntiteService {

  constructor(public http: Http) { }

  getCompte(codecompte:string){
    return this.http.get("http://localhost:8080/comptes/"+codecompte)
      .map(resp => resp.json());
  }

  getAllEntite(){
    return this.http.get("http://localhost:8080/entites")
      .map(resp => resp.json());
  }

  getEntite(nentite:string){
    return this.http.get("http://localhost:8080/entites/"+nentite)
      .map(resp => resp.json());
  }


  saveEntite(entite : Entite){
    /* return this.http.post("http://localhost:8080/entites?CodeCompte="+codecompter, entite) */
    return this.http.post("http://localhost:8080/entites", entite)
      .map(resp => resp.json());

  }

  updateEntite(entite : Entite){
    return this.http.put("http://localhost:8080/entites/"+entite.nentite, entite)
      .map(resp => resp.json());

  }

}
