import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Acheteur } from '../../model/model.acheteur';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class AcheteurService {

  constructor(public http : Http) { }

  getAllAcheteur(){
    return this.http.get("http://localhost:8080/acheteurs")
      .map(resp => resp.json());
  }

  getAcheteur(nacheteur : string){
    return this.http.get("http://localhost:8080/acheteurs/"+nacheteur)
      .map(resp => resp.json());
  }


  getAllAchComm(codeacheteur : string){
    return this.http.get("http://localhost:8080/acheteurs/getAcheteur?mc="+codeacheteur)
      .map(resp => resp.json());
  }

  saveAcheteur(acheteur : Acheteur){
    return this.http.post("http://localhost:8080/acheteurs", acheteur)
      .map(resp => resp.json());

  }

  updateAcheteur(acheteur : Acheteur){
    return this.http.put("http://localhost:8080/acheteurs/"+acheteur.matriculeacheteur, acheteur)
      .map(resp => resp.json());

  }

/*   private valueSource = new BehaviorSubject<string>("default message");
  currentValue = this.valueSource.asObservable();

  

  acheteurValue(cacheteur: string) {
    this.valueSource.next(cacheteur)

} */

}
