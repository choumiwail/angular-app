import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Suivi } from '../../model/model.suivi';

@Injectable()
export class SuiviService {

  constructor(public http : Http) { }

  getAllSuivi(){
    return this.http.get("http://localhost:8080/suivis")
      .map(resp => resp.json());
  }

  getList(){
    return this.http.get("http://localhost:8080/suivis/getsuivi")
      .map(resp => resp.json());
  }

  saveSuivi(suivi : Suivi){
    return this.http.post("http://localhost:8080/suivis", suivi)
      .map(resp => resp.json());
  }

  getSuivi(numsuivi : string){
    return this.http.get("http://localhost:8080/suivis/"+numsuivi)
      .map(resp => resp.json());
  }

  updateSuivi(suivi : Suivi){
    return this.http.put("http://localhost:8080/suivis/"+suivi.numsuivi, suivi)
      .map(resp => resp.json());
  }

  /* Search */
  findByCompte(nomcompte : string){
    return this.http.get("http://localhost:8080/suivis/findByCompte?mc="+nomcompte)
      .map(resp => resp.json());
  }

  findByEntite(nomentite : string){
    return this.http.get("http://localhost:8080/suivis/findByEntite?mc="+nomentite)
      .map(resp => resp.json());
  }

  findBySEntite(nomsentite : string){
    return this.http.get("http://localhost:8080/suivis/findBySEntite?mc="+nomsentite)
      .map(resp => resp.json());
  }

  findByClient(nomclient : string){
    return this.http.get("http://localhost:8080/suivis/findByClient?mc="+nomclient)
      .map(resp => resp.json());
  }
  /* End */

  /* getListOfCompte(codeclient : string){
    return this.http.get("http://localhost:8080/suivis/ChercherClient?mc="+codeclient)
      .map(resp => resp.json());
  }

  getListOfSuivEnt(codeclient : string,codecompte : string){
    return this.http.get("http://localhost:8080/suivis/ChercherClients?mc="+codeclient+"&codecompte="+codecompte)
      .map(resp => resp.json());
  } */

}
