import { TestBed, inject } from '@angular/core/testing';

import { PageAcceuilService } from './page-acceuil.service';

describe('PageAcceuilService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PageAcceuilService]
    });
  });

  it('should be created', inject([PageAcceuilService], (service: PageAcceuilService) => {
    expect(service).toBeTruthy();
  }));
});
