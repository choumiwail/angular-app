import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Client } from '../../model/model.client';

@Injectable()
export class ClientService {

  constructor(public http : Http) { }

  getAllClient(){
    return this.http.get("http://localhost:8080/clients")
      .map(resp => resp.json());
  }

  getClient(nclient : string){
    return this.http.get("http://localhost:8080/clients/"+nclient)
      .map(resp => resp.json());
  }

  saveClient(client : Client){
    /* return this.http.post("http://localhost:8080/entites?CodeCompte="+codecompter, entite) */
    return this.http.post("http://localhost:8080/clients", client)
      .map(resp => resp.json());

  }

  updateClient(client : Client){
    return this.http.put("http://localhost:8080/clients/"+client.matriculeclient, client)
      .map(resp => resp.json());
  }

  getListOfSE(codecompte : string){
    return this.http.get("http://localhost:8080/clients/getEntiteSous?mc="+codecompte)
      .map(resp => resp.json());
  }

  

}
