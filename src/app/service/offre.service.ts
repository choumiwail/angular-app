import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Offre } from '../../model/model.offre';

@Injectable()
export class OffreService {

  constructor(public http : Http) { }
  
  getAllOffres(){
    return this.http.get("http://localhost:8080/offres")
      .map(resp => resp.json());
  }

  getOffres(refappel : string){
    return this.http.get("http://localhost:8080/offres/"+refappel)
      .map(resp => resp.json());
  }
 
  saveOffre(offre : Offre){
    return this.http.post("http://localhost:8080/offres", offre)
      .map(resp => resp.json());
  }

  updateOffre(offre : Offre){
    return this.http.put("http://localhost:8080/offres/"+offre.refappel, offre)
      .map(resp => resp.json());

  }
}
