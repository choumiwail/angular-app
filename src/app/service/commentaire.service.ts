import { Commentaire } from './../../model/model.commentaire';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CommentaireService {

  constructor(public http : Http) { }

  getAllCommentaire(){
    return this.http.get("http://localhost:8080/commentaires")
      .map(resp => resp.json());
  }


  getCommentaire(ncommentaire : Commentaire){
    return this.http.get("http://localhost:8080/commentaires/" +ncommentaire)
      .map(resp => resp.json());
  }

  saveCommentaire(commentaire : Commentaire){
    return this.http.post("http://localhost:8080/commentaires", commentaire)
      .map(resp => resp.json());
  }

}
