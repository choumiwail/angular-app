import { TestBed, inject } from '@angular/core/testing';

import { SuiviService } from './suivi.service';

describe('SuiviService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SuiviService]
    });
  });

  it('should be created', inject([SuiviService], (service: SuiviService) => {
    expect(service).toBeTruthy();
  }));
});
