import { TestBed, inject } from '@angular/core/testing';

import { ComptabiliteService } from './comptabilite.service';

describe('ComptabiliteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ComptabiliteService]
    });
  });

  it('should be created', inject([ComptabiliteService], (service: ComptabiliteService) => {
    expect(service).toBeTruthy();
  }));
});
