import { TestBed, inject } from '@angular/core/testing';

import { CommercialService } from './commercial.service';

describe('CommercialService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommercialService]
    });
  });

  it('should be created', inject([CommercialService], (service: CommercialService) => {
    expect(service).toBeTruthy();
  }));
});
