import { SousEntite } from './../../model/model.sous-entite';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class SousEntiteService {

  constructor(public http : Http) { }

  getAllSEntite(){
    return this.http.get("http://localhost:8080/sousentites")
      .map(resp => resp.json());
  }

  getSEntite(nsentite : string){
    return this.http.get("http://localhost:8080/sousentites/"+nsentite)
      .map(resp => resp.json());
  }

  getListOfEntite(codecompte : string){
    return this.http.get("http://localhost:8080/sousentites/getEntite?mc="+codecompte)
      .map(resp => resp.json());
  }

  saveSousEntite(sousEntite : SousEntite){

    return this.http.post("http://localhost:8080/sousentites", sousEntite)
      .map(resp => resp.json());
  }

  updateSousEntite(sousEntite : SousEntite){
    return this.http.put("http://localhost:8080/sousentites/"+sousEntite.nsentite, sousEntite)
      .map(resp => resp.json());

  }


}
