import { TestBed, inject } from '@angular/core/testing';

import { EntiteService } from './entite.service';

describe('EntiteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EntiteService]
    });
  });

  it('should be created', inject([EntiteService], (service: EntiteService) => {
    expect(service).toBeTruthy();
  }));
});
