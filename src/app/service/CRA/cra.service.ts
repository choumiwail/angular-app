import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { CompteRenduActivite } from '../../../model/CRA/model.compterenduactivite';

@Injectable()
export class CraService {

  constructor(private http : Http) { }

  getAllCra(){
    return this.http.get("http://localhost:8080/compterRenduActivites")
      .map(resp => resp.json());
  }

  getCra(idcra : number){
    return this.http.get("http://localhost:8080/compterRenduActivites/"+idcra)
      .map(resp => resp.json());
  }

  saveCra(cra : CompteRenduActivite[]){
    return this.http.post("http://localhost:8080/compterRenduActivites", cra)
      .map(resp => resp.json());

  }

}
