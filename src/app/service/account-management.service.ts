import { Employeur } from './../../model/Utilisateur/model.employeur';
import { Http,Headers, RequestOptions,Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppComponent } from '../app.component';

@Injectable()
export class AccountManagementService {

  constructor(public http: Http) { }

  public logIn(employeur: Employeur) {

    let headers = new Headers();
    headers.append('Accept', 'application/json')
    // creating base64 encoded String from user name and password
    var base64Credential: string = btoa(employeur.username + ':' + employeur.password);
    headers.append("Authorization", "Basic " + base64Credential);

    

    let options = new RequestOptions();
    options.headers = headers;

    return this.http.get("http://localhost:8080/employeurs/login", options)
      .map((response: Response) => {
        // login successful if there's a jwt token in the response
        let employeur = response.json().principal;// the returned user object is a principal object
        if (employeur) {
          // store user details  in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(employeur));
        }
      });
  }

  logOut() {
    return this.http.post("http://localhost:8080" + "logout", {})
      .map((response: Response) => {
        localStorage.removeItem('currentUser');
      });

  }

  /* API_URL ="http://localhost:8080"
  logOut() {
    
    return this.http.post(this.API_URL+"logout",{})
      .map((response: Response) => {
        localStorage.removeItem('currentUser');
      });

  } */


  /* authenticated = false;

  constructor(private http: HttpClient) {
  }

  authenticate(credentials, callback) {

        const headers = new HttpHeaders(credentials ? {
            authorization : 'Basic ' + btoa(credentials.username + ':' + credentials.password)
        } : {});

        this.http.get('user', {headers: headers}).subscribe(response => {
            if (response['name']) {
                this.authenticated = true;
            } else {
                this.authenticated = false;
            }
            return callback && callback();
        });

    } */

}
