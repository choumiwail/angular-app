import { Router } from '@angular/router';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AccountManagementService } from '../service/account-management.service';
import { Employeur } from '../../model/Utilisateur/model.employeur';

@Component({
  selector: 'page-acceuil',
  templateUrl: './page-acceuil.component.html',
  styleUrls: ['./page-acceuil.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PageAcceuilComponent implements OnInit{
  commercial: any[];
  currentUser : Employeur;

  constructor(private router: Router,
              private accountManagementService : AccountManagementService) {
                
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

   }
  /*  navigate(path) {
    this.route.navigate([{outlets: {primary: path, sidemenu:path}}], 
                         {relativeTo: this.Aroute});
} */

  ngOnInit() {

}

// login out from the app
logOut(){
  this.accountManagementService.logOut()
    .subscribe(data=>{
      this.router.navigate(['/login']);
      },err=>{
      console.log(err)
      }
    )
}

}

