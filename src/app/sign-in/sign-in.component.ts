import { Router } from '@angular/router';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AccountManagementService } from '../service/account-management.service';
import { Employeur } from '../../model/Utilisateur/model.employeur';

@Component({
  selector: 'sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SignInComponent implements OnInit {
  
  employeur: Employeur=new Employeur();
  errorMessage:string;

  constructor(private router : Router,
              private accountManagementService : AccountManagementService) { }

  ngOnInit() {
  }

  /* loadPage(){
    this.router.navigate(['/acceuil']);
  } */

  login(){
    this.accountManagementService.logIn(this.employeur)
      .subscribe(data=>{
        this.router.navigate(['/acceuil']);
        },err=>{
        this.errorMessage="error :  Username or password is incorrect";
        }
      )
  }

 /*  login() {
    this.accountManagementService.authenticate(this.credentials, () => {
        this.router.navigateByUrl('/');
    });
    return false;
  } */

  /* authenticated() { return this.accountManagementService.authenticated; } */

}
