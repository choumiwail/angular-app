import { AccountManagementModule } from './sign-in/account-management/account-management.module';
import { AppRoutingComptabiliteModule } from './comptabilites/app-routing-comptabilite/app-routing-comptabilite.module';
import { ConsultantService } from './service/Comptabilite/consultant.service';
import { FactureService } from './service/Comptabilite/facture.service';
import { ContratService } from './service/Comptabilite/contrat.service';
import { EntrepriseService } from './service/Comptabilite/entreprise.service';
import { ContactService } from './service/contact.service';
import { SousEntiteService } from './service/sous-entite.service';
import { CompteService } from './service/compte.service';
import { DataService } from './service/data.service';
import { CommercialService } from './service/commercial.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';


import { AppComponent } from './app.component';
import { PageAcceuilComponent } from './page-acceuil/page-acceuil.component';
import { CommercialComponent } from './commercial/commercial.component';
import { RouterModule,Routes } from '@angular/router';


import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/* Add */
import { CommonModule } from '@angular/common';
/* Bib Bootstrap */
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PageAcceuilService } from './service/page-acceuil.service';
import { AppRoutingModule } from './/app-routing.module';
import { AnimatedButtonComponent } from './commercial/animated-button/animated-button.component';
import { CommercialElementComponent } from './commercial/commercial-element/commercial-element.component';
import { SousButtonComponent } from './commercial/sous-button/sous-button.component';
import { ToogleButtonComponent } from './commercial/toogle-button/toogle-button.component';
import { AddCommercialComponent } from './add-commercial/add-commercial.component';
import { AppRoutingCommercialModule } from './app-routing-commercial/app-routing-commercial.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommercialAcceuilComponent } from './commercial/commercial-acceuil/commercial-acceuil.component';
import { ListCompteComponent } from './commercial/list-compte/list-compte.component';
import { ListEntiteComponent } from './commercial/list-entite/list-entite.component';
import { DatepickerRangeComponent } from './datepicker-range/datepicker-range.component';
import { EntiteService } from './service/entite.service';
import { ListSousEntiteComponent } from './commercial/list-sous-entite/list-sous-entite.component';
import { ClientService } from './service/client.service';
import { AcheteurService } from './service/acheteur.service';
import { ListAcheteurComponent } from './commercial/list-acheteur/list-acheteur.component';
import { ListClientComponent } from './commercial/list-client/list-client.component';
import { SuiviService } from './service/suivi.service';
import { HeadersComponent } from './headers/headers.component';
import { BaseCommercialComponent } from './commercial/base-commercial/base-commercial.component';
import { RendezVousService } from './service/rendez-vous.service';
import { AddCompteComponent } from './commercial/add-compte/add-compte.component';
import { CommentaireService } from './service/commentaire.service';
import { SuiviRappelComponent } from './base-commerciale/suivi-rappel/suivi-rappel.component';
import { ListBaseCommercialeComponent } from './base-commerciale/list-base-commerciale/list-base-commerciale.component';
import { UpdateBaseCommercialeComponent } from './base-commerciale/update-base-commerciale/update-base-commerciale.component';
import { AddBesoinComponent } from './besoin/add-besoin/add-besoin.component';
import { PanelOffreComponent } from './besoin/panel-offre/panel-offre.component';
import { OffreService } from './service/offre.service';
import { ComptabiliteAcceuilComponent } from './comptabilites/comptabilite-acceuil/comptabilite-acceuil.component';
import { ComptabiliteComponent } from './comptabilites/comptabilite/comptabilite.component';
import { ComptabiliteElementComponent } from './comptabilites/comptabilite-element/comptabilite-element.component';
import { AnimatedButtonCptComponent } from './comptabilites/animated-button-cpt/animated-button-cpt.component';
import { SousButtonCptComponent } from './comptabilites/sous-button-cpt/sous-button-cpt.component';
import { AddParametrageComponent } from './comptabilites/CRUD_Comptabilite/add-parametrage/add-parametrage.component';
import { AddContratComponent } from './comptabilites/CRUD_Comptabilite/add-contrat/add-contrat.component';
import { AddConsultantComponent } from './comptabilites/CRUD_Comptabilite/add-consultant/add-consultant.component';
import { EmployeurService } from './service/Utilisateur/employeur.service';
import { AddFactureComponent } from './comptabilites/CRUD_Comptabilite/add-facture/add-facture.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { AccountManagementService } from './service/account-management.service';
import { UrlPermission } from './urlPermission/url.permission';
import { CraAcceuilComponent } from './compteRendu/cra-acceuil/cra-acceuil.component';
import { AppRoutingCraModule } from './compteRendu/app-routing-cra/app-routing-cra.module';
import { CraComponent } from './compteRendu/cra/cra.component';
import { BkholidayComponent } from './compteRendu/bkholiday/bkholiday.component';
import { CraService } from './service/CRA/cra.service';



@NgModule({
  declarations: [
    AppComponent,
    PageAcceuilComponent,
    CommercialComponent,
    AnimatedButtonComponent,
    CommercialElementComponent,
    SousButtonComponent,
    ToogleButtonComponent,
    AddCommercialComponent,
    CommercialAcceuilComponent,
    ListCompteComponent,
    ListEntiteComponent,
    DatepickerRangeComponent,
    ListSousEntiteComponent,
    ListAcheteurComponent,
    ListClientComponent,
    HeadersComponent,
    BaseCommercialComponent,
    AddCompteComponent,
    SuiviRappelComponent,
    ListBaseCommercialeComponent,
    UpdateBaseCommercialeComponent,
    AddBesoinComponent,
    PanelOffreComponent,
    ComptabiliteAcceuilComponent,
    ComptabiliteComponent,
    ComptabiliteElementComponent,
    AnimatedButtonCptComponent,
    SousButtonCptComponent,
    AddParametrageComponent,
    AddContratComponent,
    AddConsultantComponent,
    AddFactureComponent,
    SignInComponent,
    CraAcceuilComponent,
    CraComponent,
    BkholidayComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    /*Add Bib Bootstrap */
    CommonModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    NgbModule.forRoot(),
    AccountManagementModule,
    AppRoutingModule,
    AppRoutingCommercialModule,
    AppRoutingComptabiliteModule,
    AppRoutingCraModule,
  ],
  providers: [
    PageAcceuilService,
    CommercialService,
    DataService,
    CompteService,
    EntiteService,
    SousEntiteService,
    ClientService,
    AcheteurService,
    SuiviService,
    ContactService,
    RendezVousService,
    CommentaireService,
    OffreService,
    EntrepriseService,
    ContratService,
    FactureService,
    ConsultantService,
    EmployeurService,
    AccountManagementService,
    UrlPermission,
    CraService
  ],
  bootstrap: [AppComponent],
  
})
export class AppModule { }
