import { ListBaseCommercialeComponent } from './../base-commerciale/list-base-commerciale/list-base-commerciale.component';
import { AddCompteComponent } from './../commercial/add-compte/add-compte.component';
import { ListClientComponent } from './../commercial/list-client/list-client.component';
import { ListAcheteurComponent } from './../commercial/list-acheteur/list-acheteur.component';
import { ListSousEntiteComponent } from './../commercial/list-sous-entite/list-sous-entite.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CommercialComponent } from '../commercial/commercial.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommercialAcceuilComponent } from '../commercial/commercial-acceuil/commercial-acceuil.component';
import { ListCompteComponent } from '../commercial/list-compte/list-compte.component';
import { ListEntiteComponent } from './../commercial/list-entite/list-entite.component';
import { PageAcceuilComponent } from '../page-acceuil/page-acceuil.component';

const comCenterRoutes: Routes = [
  {
    path: 'commercial-acceuil',
    component: CommercialAcceuilComponent,
    children:[
      {path: '', redirectTo: 'commercial', pathMatch: 'full'},
      {path: 'commercial', component: CommercialComponent},
      {path: 'list-compte', component: ListCompteComponent},
      {path: 'list-entite', component: ListEntiteComponent},
      {path: 'list-sous-entite', component: ListSousEntiteComponent},
      {path: 'list-acheteur', component: ListAcheteurComponent},
      {path: 'list-client', component: ListClientComponent},
      {path: 'list-base-commerciale', component: ListBaseCommercialeComponent}
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(comCenterRoutes),
    
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingCommercialModule { }
