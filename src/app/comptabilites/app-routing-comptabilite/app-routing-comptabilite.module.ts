import { ComptabiliteAcceuilComponent } from './../comptabilite-acceuil/comptabilite-acceuil.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComptabiliteComponent } from '../comptabilite/comptabilite.component';

const compCenterRoutes: Routes = [
  {
    path: 'comptabilite-acceuil',
    component: ComptabiliteAcceuilComponent,
    children:[
      {path: '', redirectTo: 'comptabilite', pathMatch: 'full'},
      {path: 'comptabilite', component: ComptabiliteComponent},
      /* {path: 'list-compte', component: ListCompteComponent},
      {path: 'list-entite', component: ListEntiteComponent},
      {path: 'list-sous-entite', component: ListSousEntiteComponent},
      {path: 'list-acheteur', component: ListAcheteurComponent},
      {path: 'list-client', component: ListClientComponent},
      {path: 'list-base-commerciale', component: ListBaseCommercialeComponent} */
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(compCenterRoutes),
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingComptabiliteModule { }
