import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-comptabilite-element',
  templateUrl: './comptabilite-element.component.html',
  styleUrls: ['./comptabilite-element.component.css']
})
export class ComptabiliteElementComponent implements OnInit {

  @Input() title: string;
  @Input() selectorId: string;
  @Input() image: string;

  constructor() { }

  ngOnInit() {
  }

}
