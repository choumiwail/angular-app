import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComptabiliteElementComponent } from './comptabilite-element.component';

describe('ComptabiliteElementComponent', () => {
  let component: ComptabiliteElementComponent;
  let fixture: ComponentFixture<ComptabiliteElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComptabiliteElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComptabiliteElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
