import { EmployeurService } from './../../../service/Utilisateur/employeur.service';
import { ConsultantService } from './../../../service/Comptabilite/consultant.service';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Consultant } from '../../../../model/Comptabilite/model.consultant';
import { Employeur } from '../../../../model/Utilisateur/model.employeur';

@Component({
  selector: 'add-consultant',
  templateUrl: './add-consultant.component.html',
  styleUrls: ['./add-consultant.component.css']
})
export class AddConsultantComponent implements OnInit {

  @Output() event = new EventEmitter<any>();
  currentUser : Employeur;

  values = '';


  constructor(private consultantService : ConsultantService,
              private employeurService : EmployeurService) {

                this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
               }

  ngOnInit() {
    this.getListEmp();
    this.values = this.currentUser.username;
  }

  closeToogle(){
    this.event.emit();
  }

  saveConsultant(consultant : Consultant){
    this.consultantService.saveConsultant(consultant)
      .subscribe(data => {
        console.log(data)
      })
  }

  pageEmployeur : any;
  getListEmp(){
    this.employeurService.getAllEmployeur()
      .subscribe(data => {
        console.log(data)
        this.pageEmployeur = data
      }, err => {
        console.log(err)
      })
  }
}
