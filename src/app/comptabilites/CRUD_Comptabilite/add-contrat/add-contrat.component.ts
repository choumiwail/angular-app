import { ContratService } from './../../../service/Comptabilite/contrat.service';
import { OffreService } from './../../../service/offre.service';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Offre } from '../../../../model/model.offre';
import { Contrat } from '../../../../model/Comptabilite/model.contrat';

@Component({
  selector: 'add-contrat',
  templateUrl: './add-contrat.component.html',
  styleUrls: ['./add-contrat.component.css']
})
export class AddContratComponent implements OnInit {

  @Output() event = new EventEmitter<any>();
  @Output() eventacc = new EventEmitter<any>();
  
  constructor(public offreService : OffreService,
              public contratService : ContratService) {
               }

  ngOnInit() {
    this.getAllOffres()
  }

  closeToogle(){
    this.event.emit();
  }

  openToogle(){
    this.eventacc.emit();
  }

  pageOffre : any;

  getAllOffres(){
    this.offreService.getAllOffres()
      .subscribe(data => {
        this.pageOffre = data;
        console.log(data);
      }, err => {
        console.log(err);
      });
  }

  nappel : any; 
  pgoffre : Offre = new Offre();
  getOffres(){
    console.log(this.nappel);
    console.log("Console : "+this.nappel);
    this.offreService.getOffres(this.nappel)
      .subscribe(data => {
        console.log("------ : "+this.nappel);
        this.pgoffre = data;
        console.log(this.pgoffre.refappel)
      })
  }


  /* Method of class Contrat*/
  saveContrat(contrat : Contrat){
    console.log("***contrat***")
    this.contratService.saveContrat(contrat)
      .subscribe(data => {
        console.log("***contrat 1***")
        console.log(data)
      }, err => {
        console.log(err)
      })
  }
  /* End */

}
