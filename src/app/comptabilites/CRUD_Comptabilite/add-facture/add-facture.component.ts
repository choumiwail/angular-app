import { FactureService } from './../../../service/Comptabilite/facture.service';
import { OffreService } from './../../../service/offre.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Offre } from '../../../../model/model.offre';
import { Facture } from '../../../../model/Comptabilite/model.facture';
import { ContratService } from '../../../service/Comptabilite/contrat.service';
import { Contrat } from '../../../../model/Comptabilite/model.contrat';

@Component({
  selector: 'add-facture',
  templateUrl: './add-facture.component.html',
  styleUrls: ['./add-facture.component.css']
})
export class AddFactureComponent implements OnInit {

  @Output() event = new EventEmitter<any>();
  @Output() eventclose = new EventEmitter<any>();
  today =  Date.now();
  tvas = 1.2;
  constructor(public offreService : OffreService,
              public contratService : ContratService,
              public factureService : FactureService) { }

  ngOnInit() {
    this.getAllContrats()
  }

  closeToogle(){
    this.event.emit();
  }

  clsToogle(){
    this.eventclose.emit();
  }

  pageContrat : any;
  getAllContrats(){
    this.contratService.getAllContrat()
      .subscribe(data => {
        console.log(data)
        this.pageContrat = data
      }, err => {
        console.log(err)
      })
  }

  ncontrats : any;
  pgcontrat : Contrat = new Contrat();
  getListOfContrat(){
    this.contratService.getContrat(this.ncontrats)
      .subscribe(data => {
        console.log(data)
        this.pgcontrat = data
      }, err => {
        console.log(err)
      })
  }

  saveFacture(facture : Facture){
    this.factureService.saveFacture(facture)
      .subscribe(data => {
        console.log(data)
      }, err => {
        console.log(err)
      })
  }

}
