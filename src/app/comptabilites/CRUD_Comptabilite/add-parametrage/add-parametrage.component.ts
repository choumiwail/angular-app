import { Entreprise } from './../../../../model/Comptabilite/model.entreprise';
import { EntrepriseService } from './../../../service/Comptabilite/entreprise.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'add-parametrage',
  templateUrl: './add-parametrage.component.html',
  styleUrls: ['./add-parametrage.component.css']
})
export class AddParametrageComponent implements OnInit {

  @Output() event = new EventEmitter<any>();
  @Output() eventclose = new EventEmitter<any>();
  constructor(private entrepriseService : EntrepriseService) { }

  ngOnInit() {
  }

  closeToogle(){
    this.event.emit();
  }

  clsToogle(){
    this.eventclose.emit();
  }

   /* Method of class Entreprise*/
  saveEntreprise(entreprise : Entreprise){
    this.entrepriseService.saveEntreprise(entreprise)
      .subscribe(data => {
        console.log(data)
        /* this.router.navigate(['/commercial-acceuil/list-compte']); */
      }, err => {
        console.log(err);
      })
  }
   /* End */
}
