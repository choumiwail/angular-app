import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComptabiliteAcceuilComponent } from './comptabilite-acceuil.component';

describe('ComptabiliteAcceuilComponent', () => {
  let component: ComptabiliteAcceuilComponent;
  let fixture: ComponentFixture<ComptabiliteAcceuilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComptabiliteAcceuilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComptabiliteAcceuilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
