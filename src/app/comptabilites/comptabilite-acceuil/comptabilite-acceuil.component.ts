import { Component, OnInit } from '@angular/core';
import { Employeur } from '../../../model/Utilisateur/model.employeur';

@Component({
  selector: 'comptabilite-acceuil',
  templateUrl: './comptabilite-acceuil.component.html',
  styleUrls: ['./comptabilite-acceuil.component.css']
})
export class ComptabiliteAcceuilComponent implements OnInit {
  currentUser : Employeur;

  constructor() { 
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
  }

}
