import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimatedButtonCptComponent } from './animated-button-cpt.component';

describe('AnimatedButtonCptComponent', () => {
  let component: AnimatedButtonCptComponent;
  let fixture: ComponentFixture<AnimatedButtonCptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnimatedButtonCptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimatedButtonCptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
