import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-animated-button-cpt',
  templateUrl: './animated-button-cpt.component.html',
  styleUrls: ['./animated-button-cpt.component.css']
})
export class AnimatedButtonCptComponent implements OnInit , AfterViewInit {
  @Input() selectorId: string;
  @Input() materialBtnTog: string;
  @Input() option: string;
  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    $(`.${this.materialBtnTog}`).on("click", () => {
      $(this).toggleClass('open');
      $(`.${this.option}`).toggleClass('scale-on');
  });
  }
}
