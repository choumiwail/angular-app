import { DataService } from './../../service/data.service';
import { Component, OnInit, Input, ViewChild } from '@angular/core';

@Component({
  selector: 'app-sous-button-cpt',
  templateUrl: './sous-button-cpt.component.html',
  styleUrls: ['./sous-button-cpt.component.css']
})
export class SousButtonCptComponent implements OnInit {

  @ViewChild('contentparametrage') readonly createParametrageModal;
  @ViewChild('contentcontrat') readonly createContratModal;
  @ViewChild('contentfacture') readonly createFactureModal;
  
  
  @Input() optionClass: string;
  @Input() options: string;
  @Input() selectorId: string;

  ii = [{ id: 1, name: "fa fa-table" },
  { id: 2, name: "fa fa-plus-square" },
  { id: 3, name: "fa fa-file-text-o" },];

  constructor(public dataService : DataService) { }

  ngOnInit() {
  }

  chekedBtn(selectorIds, typeOfCommmercial) {
    let index = this.ii[selectorIds];
    console.log(this.ii[selectorIds]);
    if (index.id === 2 && typeOfCommmercial === 'parametrage') {
       this.dataService.open(this.createParametrageModal);
       /*this.data.currentValue.subscribe(count => this.count = count) */
              
    }
    if (index.id === 1 && typeOfCommmercial === 'parametrage') {
      /* this.router.navigate(['/commercial-acceuil/list-compte']); */
      
    }
    if (index.id === 2 && typeOfCommmercial === 'contrat') {
       this.dataService.open(this.createContratModal);
      /*this.getListCompte();
      this.data.currentValue.subscribe(count => this.count = count) */
    }
    if (index.id === 1 && typeOfCommmercial === 'contrat') {
      /* this.router.navigate(['/commercial-acceuil/list-entite']); */
      

    }
    if (index.id === 2 && typeOfCommmercial === 'facture') {
       this.dataService.open(this.createFactureModal);
      /*this.getListCompte();
      this.data.currentValue.subscribe(count => this.count = count) */

    }
    if (index.id === 1 && typeOfCommmercial === 'facture') {
      /* this.router.navigate(['/commercial-acceuil/list-sous-entite']); */

    }
  
  }

}
