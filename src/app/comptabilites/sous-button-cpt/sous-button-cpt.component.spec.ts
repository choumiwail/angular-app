import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SousButtonCptComponent } from './sous-button-cpt.component';

describe('SousButtonCptComponent', () => {
  let component: SousButtonCptComponent;
  let fixture: ComponentFixture<SousButtonCptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SousButtonCptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SousButtonCptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
