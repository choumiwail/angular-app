import { DataService } from './../../service/data.service';
import { SousEntiteService } from './../../service/sous-entite.service';
import { SuiviService } from './../../service/suivi.service';
import { ClientService } from './../../service/client.service';
import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { AcheteurService } from '../../service/acheteur.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'list-base-commerciale',
  templateUrl: './list-base-commerciale.component.html',
  styleUrls: ['./list-base-commerciale.component.css']
})
export class ListBaseCommercialeComponent implements OnInit,AfterViewInit {

  count : number;
  @ViewChild('contentBD') readonly createBDModal;
   closeResult: string;
   nomcompte : string = "";
   showComptes:boolean = true;
   showEntites:boolean = false;
   showSEntites:boolean = false;
   showClients:boolean = false;

  constructor(public http : Http,
              public suiviService : SuiviService,
              public clientService : ClientService,
              public sEntiteService : SousEntiteService,
              public dataService : DataService,
              private modalService: NgbModal) { }

  ngOnInit() {
    this.dataService.currentValue.subscribe(count => this.count = count)
  }

  ngAfterViewInit(){
    this.getAllSuivi();
    this.getListSEntite();
    this.getAllS();
    this.doSearchCpt();
    this.doSearchEnt();
    this.doSearchSent();
    this.doSearchClient();
  }

  newValue() {
    this.dataService.changeValue(this.count)
   }

  pageSEntites : any;
  getListSEntite(){
    this.sEntiteService.getAllSEntite()
      .subscribe(data => {
        this.pageSEntites = data;
        console.log(data);
      }, err => {
        console.log(err);
      });
  }

  pageSuivis :any;
  getAllSuivi(){
    this.suiviService.getAllSuivi()
      .subscribe(data => {
        this.pageSuivis = data;
        console.log(data);
      }, err => {
        console.log(err);
      });
  }

  pgsSuivis :any;
  getAllS(){
    this.suiviService.getList()
      .subscribe(data => {
        this.pgsSuivis = data;
        console.log(data);
      }, err => {
        console.log(err);
      });
  }

  openContent(){
    this.open(this.createBDModal);
  }

  open(content) {
    this.modalService.open(content,{ size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
 
  /* Search by criterion */
  pgComptes : any;
  page : number = 0;
  size : number = 5;
  pages : Array<number>;
  doSearchCpt(){
    this.suiviService.findByCompte(this.nomcompte)
      .subscribe(data => {
        this.pgComptes = data;
        this.pages = new Array(data.totalPages);
        console.log("data of chercher" + this.pgComptes)
        console.log("data of pg" + this.pages)
      }, err => {
        console.log(err);
      });
  }

  findByCompte(){
    this.showComptes = true;
    this.showEntites = false;
    this.showSEntites = false;
    this.showClients = false;
     this.doSearchCpt();
  }

  nomentite : string ="";
  pgEntites   : any;
  doSearchEnt(){
    this.suiviService.findByEntite(this.nomentite)
      .subscribe(data => {
        this.pgEntites = data;
      }, err => {
        console.log(err);
      });
  }

  findByEntite(){
    this.showComptes = false;
    this.showEntites = true;
    this.showSEntites = false;
    this.showClients = false;
     this.doSearchEnt();
  }

  nomsentite : string ="";
  pgSEntites   : any;
  doSearchSent(){
    this.suiviService.findBySEntite(this.nomsentite)
      .subscribe(data => {
        this.pgSEntites = data;
      }, err => {
        console.log(err);
      });
  }

  findBySEntite(){
    this.showComptes = false;
    this.showEntites = false;
    this.showSEntites = true;
    this.showClients = false;
     this.doSearchSent();
  }

  nomclient : string ="";
  pgClients   : any;
  doSearchClient(){
    this.suiviService.findByClient(this.nomclient)
      .subscribe(data => {
        this.pgClients = data;
      }, err => {
        console.log(err);
      });
  }

  findByClient(){
    this.showComptes = false;
    this.showEntites = false;
    this.showSEntites = false;
    this.showClients = true;
     this.doSearchClient();
  }
  
  /* End */

}
