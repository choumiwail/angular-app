import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListBaseCommercialeComponent } from './list-base-commerciale.component';

describe('ListBaseCommercialeComponent', () => {
  let component: ListBaseCommercialeComponent;
  let fixture: ComponentFixture<ListBaseCommercialeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListBaseCommercialeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListBaseCommercialeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
