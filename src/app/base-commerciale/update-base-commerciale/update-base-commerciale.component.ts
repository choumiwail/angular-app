import { Component, OnInit, Input } from '@angular/core';
import { Http } from '@angular/http';

@Component({
  selector: 'update-base-commerciale',
  templateUrl: './update-base-commerciale.component.html',
  styleUrls: ['./update-base-commerciale.component.css']
})
export class UpdateBaseCommercialeComponent implements OnInit {

  @Input() childData: any;
  constructor(public http : Http) { }

  ngOnInit() {
    /* console.log("child data: "+ this.childData[0][0]) */
  }

}
