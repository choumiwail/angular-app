import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateBaseCommercialeComponent } from './update-base-commerciale.component';

describe('UpdateBaseCommercialeComponent', () => {
  let component: UpdateBaseCommercialeComponent;
  let fixture: ComponentFixture<UpdateBaseCommercialeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateBaseCommercialeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateBaseCommercialeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
