import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuiviRappelComponent } from './suivi-rappel.component';

describe('SuiviRappelComponent', () => {
  let component: SuiviRappelComponent;
  let fixture: ComponentFixture<SuiviRappelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuiviRappelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuiviRappelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
