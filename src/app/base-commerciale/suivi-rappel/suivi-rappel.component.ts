import { DataService } from './../../service/data.service';
import { Component, OnInit, OnChanges, Output, EventEmitter, Input, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { Suivi } from '../../../model/model.suivi';
import { SuiviService } from '../../service/suivi.service';

@Component({
  selector: 'suivi-rappel',
  templateUrl: './suivi-rappel.component.html',
  styleUrls: ['./suivi-rappel.component.css']
})
export class SuiviRappelComponent implements OnInit/* ,OnChanges */,AfterViewInit {

  @Output() event = new EventEmitter<any>();

  @Output() getSuivi : EventEmitter<Suivi> = new EventEmitter<Suivi>();

  

  constructor(public suiviService : SuiviService) { }
 
  ngOnInit() {
    console.log("++++++")
    this.getAllSuivi();
    
  }

  /* ngOnChanges(){
    console.log("******")
    this.getAllSuivi();
  } */

  ngAfterViewInit(){
    console.log("method of after view")
    this.getAllSuivi();
  }
  
  openToogle(){
    this.event.emit();
  }
  
  
  /* Suivi */
  pageSuivis : any;

  getAllSuivi(){
    this.suiviService.getAllSuivi()
      .subscribe(data => {
        this.pageSuivis = data;
        console.log(data);
      }, err => {
        console.log(err);
      });
  }

  sendSuivi(pagSuivi){
    /* console.log("codesuivi : "+this.codesuivi);
    this.suiviService.getSuivi(this.codesuivi)
      .subscribe(data => {
        console.log("---codesuivi--- : "+this.codesuivi);
        pagSuivi = data;
        console.log("***Objet before suivi***"+pagSuivi.numsuivi);
        console.log("***Objet after suivi***"+pagSuivi.numsuivi);
      }), */this.getSuivi.emit(pagSuivi);console.log("***Objet after emit***"+pagSuivi.numsuivi);
  }

  /* End */
  

}
