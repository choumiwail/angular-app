import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelOffreComponent } from './panel-offre.component';

describe('PanelOffreComponent', () => {
  let component: PanelOffreComponent;
  let fixture: ComponentFixture<PanelOffreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelOffreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelOffreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
