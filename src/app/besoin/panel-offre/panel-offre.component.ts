import { Offre } from './../../../model/model.offre';
import { OffreService } from './../../service/offre.service';
import { Client } from './../../../model/model.client';
import { EntiteService } from './../../service/entite.service';
import { CompteService } from './../../service/compte.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ClientService } from '../../service/client.service';
import { SousEntiteService } from '../../service/sous-entite.service';

@Component({
  selector: 'panel-offre',
  templateUrl: './panel-offre.component.html',
  styleUrls: ['./panel-offre.component.css']
})
export class PanelOffreComponent implements OnInit {

  @Output() event = new EventEmitter<any>();

  constructor(public clientService : ClientService,
              public compteService : CompteService,
              public entiteService : EntiteService,
              public sousEntiteService : SousEntiteService,
              public offreService : OffreService) { }

  ngOnInit() {
    this.getListClient();
  }

  closeToogle(){
    this.event.emit();
  }

  /* Method of gettin information of client/Acheteur/Compte/Entite/SousEntite */
  pageClients : any;

  getListClient(){
    this.clientService.getAllClient()
      .subscribe(data => {
        this.pageClients = data;
        console.log(data);
      }, err => {
        console.log(err);
      });
  }

   getNumClient : any; 
  pageOfClients : Client = new Client();
  getClient(){
    console.log(this.getNumClient);
    console.log("Console : "+this.getNumClient);
    this.clientService.getClient(this.getNumClient)
      .subscribe(data => {
        console.log("------ : "+this.getNumClient);
        console.log("---data--- : "+data);
        this.pageOfClients = data;
        console.log(this.pageOfClients.compte.codecompte)
      })
  }


   getNumCompte : any;
  pageOfComptes : any;

  getEntite(){
    console.log("My input: ", this.getNumCompte);
    this.sousEntiteService.getListOfEntite(this.getNumCompte)
      .subscribe(data => {
        this.pageOfComptes= data;
      }, err => {
        console.log(err);
      })
  }

  getNumEntite : any;
  pageOfSE : any;

  getSEntite(){
    console.log("My input: ", this.getNumEntite);
    this.clientService.getListOfSE(this.getNumEntite)
      .subscribe(data => {
        this.pageOfSE= data;
      }, err => {
        console.log(err);
      })
  }
  /* End */

  /* Method of class Offre */
  saveOffre(offre : Offre){
    this.offreService.saveOffre(offre)
      .subscribe(data => {
        console.log(data)
      }, err => {
        console.log(err)
      })
  }

  /* End */


}
