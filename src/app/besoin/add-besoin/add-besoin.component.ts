import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'add-besoin',
  templateUrl: './add-besoin.component.html',
  styleUrls: ['./add-besoin.component.css']
})
export class AddBesoinComponent implements OnInit {

  @Output() event = new EventEmitter<any>();
  @Output() eventclose = new EventEmitter<any>();
  
  constructor() { }

  ngOnInit() {
  }

  closeToogle(){
    this.event.emit();
  }

  clsToogle(){
    this.eventclose.emit();
  }

}
