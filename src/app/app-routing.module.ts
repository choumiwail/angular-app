import { AppRoutingComptabiliteModule } from './comptabilites/app-routing-comptabilite/app-routing-comptabilite.module';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { CommercialComponent } from './commercial/commercial.component';
import { PageAcceuilComponent } from './page-acceuil/page-acceuil.component';
import { AppRoutingCommercialModule } from './app-routing-commercial/app-routing-commercial.module';


const routes: Routes = [
  { path: '', redirectTo: '/acceuil', pathMatch: 'full' },
  { path: 'acceuil', component: PageAcceuilComponent },
];

/* const comCenterRoutes: Routes = [
  {
    path: 'home',
    component: HomePageComponent,
    children:[
      {path: '', redirectTo: 'acceuil', pathMatch: 'full'},
      {path: 'acceuil', component: PageAcceuilComponent}
    ]
  }
]; */


@NgModule({
  imports: [ RouterModule.forRoot(routes,{ enableTracing: true } ), AppRoutingCommercialModule  /* RouterModule.forChild(comCenterRoutes) */] ,
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
