import { Client } from "./model.client";
import { Compte } from "./model.compte";
import { Entite } from "./model.entite";
import { SousEntite } from "./model.sous-entite";
import { Acheteur } from "./model.acheteur";

export class Offre {
          refappel : string ="";
          intitule : string = "";
		  dateemission : Date; 
		  dateexpriration : Date;
		  datedemarrage : Date;
		  datecloture : Date;
		  lieu : string;
		  nbrtravaill : number;
		  sujet : string ="";
		  profil : string ="";
		  experience : string ="";
		  competencecle : string ="";
		  competencefon : string ="";
		  competencetech : string ="";
		  objectif : string ="";
		  prestation : string ="";
		  autreinfo : string ="";
		  langue1 : string ="";
		  langue2 : string ="";
          langue3 : string ="";
          client : Client = new Client();
          compte : Compte = new Compte();
          entite : Entite = new Entite();
          sousentite : SousEntite = new SousEntite();
          acheteur : Acheteur = new Acheteur();
}