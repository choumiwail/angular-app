import { Offre } from './../model.offre';
import { Acheteur } from './../model.acheteur';
import { Client } from './../model.client';
import { Employeur } from '../Utilisateur/model.employeur';

export class Contrat {
    ncontrat : string ="";
    natureprestation : string ="";
    adresse : string ="";
    ville : string ="";
    prixjrht : any;
    frefacture : string ="";
    datecloture : Date;
    
    employeur : Employeur = new Employeur();

    client : Client = new Client();
    acheteur : Acheteur = new Acheteur();
    offre : Offre = new Offre();
}