export class Entreprise {
    nomentreprise : string =""; 
    tel : any;
    tel1 : any;
    email : string ="";
    adresse : string ="";
    rcs : string ="";
    codeape : string ="";
    tva : string="";
    nsiret : string ="";
    penalite : any;
    indeminite : any;
    nombanque : string ="";
    rib : string ="";
    iban : string ="";
    bic : string ="";
    
    /* employeur = Employeur = new this.employeur(); */
    
}