import { Compte } from "./model.compte";
import { Acheteur } from "./model.acheteur";

export class Client {
    matriculeclient : string ="";
    nom : string ="";
    prenom : string ="";
    fonction : string ="";
    adresse : string ="";
	codepostal : number =0;
    ville : string ="";
    pays : string ="";
    email : string ="";
    email1 : string ="";
    tel : number =0;
	tel1 : number =0;
	datecreation : Date;
	etat : string ="";
    direction : string ="";
    hierarchie : string = "";
    hierarchis : string = "";
    ficheclient : string = "";
    activiteclient : string = "";
    portableperso : number = 0;
    portablepro : number = 0;
    acheteur : Acheteur = new Acheteur();
    compte : Compte = new Compte();
}