import { Employeur } from "../Utilisateur/model.employeur";

export class CompteRenduActivite{
    jrs : number;
    mois : string ="";
    annee : number;
    commam : string ="";
    commpm : string ="";
    nbrheure : number;
    am : string ="";
    pm : string ="";
    mission : string ="";

    employeur : Employeur = new Employeur();

    constructor(jrs,mois,annee,commam,commpm,nbrheure,am,pm,mission,employeur){
        this.jrs=jrs;
        this.mois=mois;
        this.annee=annee;
        this.commam=commam;
        this.commpm=commpm;
        this.nbrheure=nbrheure;
        this.am=am;
        this.pm=pm;
        this.mission=mission;
        this.employeur=employeur;
    }
}